import firebase from 'firebase';
var AuthService = /** @class */ (function () {
    function AuthService() {
    }
    AuthService.prototype.signup = function (email, password) {
        return firebase.auth().createUserWithEmailAndPassword(email, password);
    };
    AuthService.prototype.login = function (email, password) {
        return firebase.auth().signInWithEmailAndPassword(email, password);
    };
    AuthService.prototype.logout = function () {
        return firebase.auth().signOut();
    };
    return AuthService;
}());
export { AuthService };
//# sourceMappingURL=auth.js.map