import { Component,ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { NavController, MenuController, LoadingController, AlertController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { Events } from 'ionic-angular';

// import {ViewController} from 'ionic-angular';

import { ProfilePage } from '../pages/profile/profile';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
// import { ProfilePage } from '../pages/profile/profile';
import { IntroPage } from '../pages/intro/intro';
import { RegisterPage} from '../pages/register/register';
import { MypetPage} from '../pages/mypet/mypet';
import { MyitemPage} from '../pages/myitem/myitem';
import { AddpetPage} from '../pages/addpet/addpet';




@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = IntroPage;
  profilePage = ProfilePage;
  registerPage = RegisterPage;
  loginPage = LoginPage;
  tabPage = TabsPage;
  introPage = IntroPage;
  mypetPage = MypetPage;
  myitemPage = MyitemPage;
  isSeller = false;
  isAuthenticated:boolean = false;

  @ViewChild('mycontent') nav: NavController;



  constructor(platform: Platform, 
              statusBar: StatusBar, 
              splashScreen: SplashScreen,
              private menuCtrl: MenuController,
              // public authService: AuthService,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              private storage: Storage,
              public events: Events
              // private viewCtrl: ViewController
              ) {

     events.subscribe('user.login', (condition) => {
        this.isAuthenticated = condition;
      });     

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

     
    });
    
  }

  onLoad(page: any) {
    this.nav.push(page);
    this.menuCtrl.close();
  }

  logout(){
    const loading = this.loadingCtrl.create({
      content: 'Logging you out...'
    });
      this.events.publish('user.login', false);
      this.storage.remove('currentUser')
      .then( data => {
        loading.dismiss();
        const alert = this.alertCtrl.create({
            title: 'Logout',
            message: 'You have logout',
            buttons: ['ok']
          });
        alert.present();
        this.nav.setRoot(this.nav.getActive().component);
      })
      .catch( error => {
        loading.dismiss();
        const alert = this.alertCtrl.create({
        title: 'You already logout',
        message: error.message,
        buttons: ['ok']
      });
        this.nav.setRoot(this.nav.getActive().component);
      alert.present();
      
      });
  }

}
