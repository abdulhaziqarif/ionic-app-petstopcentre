var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
// import { HTTPModule } from '@ionic-native/http';
import { NewsPage } from '../pages/news/news';
import { PetPage } from '../pages/pet/pet';
import { ClinicPage } from '../pages/clinic/clinic';
import { ShelterPage } from '../pages/shelter/shelter';
import { TabsPage } from '../pages/tabs/tabs';
import { IntroPage } from '../pages/intro/intro';
import { LoginPage } from '../pages/login/login';
import { SearchpetPage } from '../pages/searchpet/searchpet';
import { SearchclinicPage } from '../pages/searchclinic/searchclinic';
import { SearchshelterPage } from '../pages/searchshelter/searchshelter';
import { PetdetailPage } from '../pages/petdetail/petdetail';
import { ClinicdetailPage } from '../pages/clinicdetail/clinicdetail';
import { ShelterdetailPage } from '../pages/shelterdetail/shelterdetail';
import { ProfilePage } from '../pages/profile/profile';
import { RegisterPage } from '../pages/register/register';
import { ItemPage } from '../pages/item/item';
import { ItemdetailPage } from '../pages/itemdetail/itemdetail';
import { SearchitemPage } from '../pages/searchitem/searchitem';
import { MypetPage } from '../pages/mypet/mypet';
import { AddpetPage } from '../pages/addpet/addpet';
import { MyitemPage } from '../pages/myitem/myitem';
import { AdditemPage } from '../pages/additem/additem';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera } from '@ionic-native/camera';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { FileTransfer } from '@ionic-native/file-transfer';
// import { File } from '@ionic-native/file';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                MyApp,
                NewsPage,
                PetPage,
                ClinicPage,
                ShelterPage,
                TabsPage,
                IntroPage,
                LoginPage,
                SearchpetPage,
                SearchclinicPage,
                SearchshelterPage,
                PetdetailPage,
                ClinicdetailPage,
                ShelterdetailPage,
                ProfilePage,
                RegisterPage,
                ItemPage,
                ItemdetailPage,
                SearchitemPage,
                MypetPage,
                AddpetPage,
                MyitemPage,
                AdditemPage
            ],
            imports: [
                BrowserModule,
                IonicModule.forRoot(MyApp),
                HttpModule,
                IonicStorageModule.forRoot()
            ],
            bootstrap: [IonicApp],
            entryComponents: [
                MyApp,
                NewsPage,
                PetPage,
                ClinicPage,
                ShelterPage,
                TabsPage,
                IntroPage,
                LoginPage,
                SearchpetPage,
                SearchclinicPage,
                SearchshelterPage,
                PetdetailPage,
                ClinicdetailPage,
                ShelterdetailPage,
                ProfilePage,
                RegisterPage,
                ItemPage,
                ItemdetailPage,
                SearchitemPage,
                MypetPage,
                AddpetPage,
                MyitemPage,
                AdditemPage
            ],
            providers: [
                StatusBar,
                SplashScreen,
                Camera,
                LaunchNavigator,
                FileTransfer,
                { provide: ErrorHandler, useClass: IonicErrorHandler }
            ]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map