var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NavController, MenuController, LoadingController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';
// import {ViewController} from 'ionic-angular';
import { ProfilePage } from '../pages/profile/profile';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
// import { ProfilePage } from '../pages/profile/profile';
import { IntroPage } from '../pages/intro/intro';
import { RegisterPage } from '../pages/register/register';
import { MypetPage } from '../pages/mypet/mypet';
import { MyitemPage } from '../pages/myitem/myitem';
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, menuCtrl, 
    // public authService: AuthService,
    loadingCtrl, alertCtrl, storage, events
    // private viewCtrl: ViewController
    ) {
        var _this = this;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.events = events;
        this.rootPage = IntroPage;
        this.profilePage = ProfilePage;
        this.registerPage = RegisterPage;
        this.loginPage = LoginPage;
        this.tabPage = TabsPage;
        this.introPage = IntroPage;
        this.mypetPage = MypetPage;
        this.myitemPage = MyitemPage;
        this.isSeller = false;
        this.isAuthenticated = false;
        events.subscribe('user.login', function (condition) {
            _this.isAuthenticated = condition;
        });
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp.prototype.onLoad = function (page) {
        this.nav.push(page);
        this.menuCtrl.close();
    };
    MyApp.prototype.logout = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Logging you out...'
        });
        this.events.publish('user.login', false);
        this.storage.remove('currentUser')
            .then(function (data) {
            loading.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Logout',
                message: 'You have logout',
                buttons: ['ok']
            });
            alert.present();
            _this.nav.setRoot(_this.nav.getActive().component);
        })
            .catch(function (error) {
            loading.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'You already logout',
                message: error.message,
                buttons: ['ok']
            });
            _this.nav.setRoot(_this.nav.getActive().component);
            alert.present();
        });
    };
    __decorate([
        ViewChild('mycontent'),
        __metadata("design:type", NavController)
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Component({
            templateUrl: 'app.html'
        }),
        __metadata("design:paramtypes", [Platform,
            StatusBar,
            SplashScreen,
            MenuController,
            LoadingController,
            AlertController,
            Storage,
            Events
            // private viewCtrl: ViewController
        ])
    ], MyApp);
    return MyApp;
}());
export { MyApp };
//# sourceMappingURL=app.component.js.map