import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
// import { HTTPModule } from '@ionic-native/http';


import { NewsPage } from '../pages/news/news';
import { PetPage } from '../pages/pet/pet';
import { ClinicPage } from '../pages/clinic/clinic';


import { ShopPage } from '../pages/shop/shop';

import { ShelterPage } from '../pages/shelter/shelter';
import { TabsPage } from '../pages/tabs/tabs';
import { IntroPage } from '../pages/intro/intro';
import { LoginPage } from '../pages/login/login';
import { SearchpetPage} from '../pages/searchpet/searchpet';
import { SearchclinicPage} from '../pages/searchclinic/searchclinic';
import { SearchshelterPage} from '../pages/searchshelter/searchshelter';
import { PetdetailPage } from '../pages/petdetail/petdetail';
import { ClinicdetailPage } from '../pages/clinicdetail/clinicdetail';


import { ShopdetailPage } from '../pages/shopdetail/shopdetail';

import { ShelterdetailPage } from '../pages/shelterdetail/shelterdetail';
import { ProfilePage } from '../pages/profile/profile';
import { RegisterPage } from '../pages/register/register';
import { ItemPage } from '../pages/item/item';
import { ItemdetailPage } from '../pages/itemdetail/itemdetail';

import { ServicePage } from '../pages/service/service';
import { ServicedetailPage } from '../pages/servicedetail/servicedetail';

import { SearchitemPage} from '../pages/searchitem/searchitem';
import { MypetPage} from '../pages/mypet/mypet';
import { AddpetPage} from '../pages/addpet/addpet';
import { MyitemPage} from '../pages/myitem/myitem';

import { MyservicePage} from '../pages/myservice/myservice';

import { AdditemPage} from '../pages/additem/additem';

import { AddservicePage} from '../pages/addservice/addservice';

import { UpdatepetPage} from '../pages/updatepet/updatepet';
import { UpdateitemPage} from '../pages/updateitem/updateitem';

import { UpdateservicePage} from '../pages/updateservice/updateservice';




import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera } from '@ionic-native/camera';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { FileTransfer } from '@ionic-native/file-transfer';
import { Geolocation } from '@ionic-native/geolocation';
import { CallNumber } from '@ionic-native/call-number';
// import { File } from '@ionic-native/file';

@NgModule({
  declarations: [
    MyApp,
    NewsPage,
    PetPage,
    ClinicPage,
    ShelterPage,
    TabsPage,
    IntroPage,
    LoginPage,
    SearchpetPage,
    SearchclinicPage,
    SearchshelterPage,
    PetdetailPage,
    ClinicdetailPage,
    ShelterdetailPage,
    ProfilePage,
    RegisterPage,
    ItemPage,
    ItemdetailPage,
    SearchitemPage,
    MypetPage,
    AddpetPage,
    MyitemPage,
    AdditemPage,
    UpdatepetPage,
    UpdateitemPage,
    AddservicePage,
    UpdateservicePage,
    MyservicePage,
    ServicePage,
    ServicedetailPage,
    ShopPage,
    ShopdetailPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    NewsPage,
    PetPage,
    ClinicPage,
    ShelterPage,
    TabsPage,
    IntroPage,
    LoginPage,
    SearchpetPage,
    SearchclinicPage,
    SearchshelterPage,
    PetdetailPage,
    ClinicdetailPage,
    ShelterdetailPage,
    ProfilePage,
    RegisterPage,
    ItemPage,
    ItemdetailPage,
    SearchitemPage,
    MypetPage,
    AddpetPage,
    MyitemPage,
    AdditemPage,
    UpdatepetPage,
    UpdateitemPage,
    AddservicePage,
    UpdateservicePage,
    MyservicePage,
    ServicePage,
    ServicedetailPage,
    ShopPage,
    ShopdetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    LaunchNavigator,
    FileTransfer,
    Geolocation,
    CallNumber,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
