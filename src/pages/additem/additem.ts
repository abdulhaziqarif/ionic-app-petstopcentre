import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ActionSheetController  } from 'ionic-angular';

import { NgForm } from '@angular/forms';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

@Component({
  selector: 'page-additem',
  templateUrl: 'additem.html',
})
export class AdditemPage {
user_id :any;
image:string;
imageURI:any;
baseURL = 'http://petcentre.haziqarif.review/';
  constructor(	public navCtrl: NavController, 
                public navParams: NavParams,
  	 			private http: Http, 
              	public loadingCtrl : LoadingController,
             	public alertCtrl : AlertController,
                public storage: Storage,
                public actionSheetCtrl: ActionSheetController,
                private camera: Camera,
                private transfer: FileTransfer ) {
    this.storage.get('currentUser').then((val) => {
          this.user_id = val.id;
          console.log(this.user_id);
        });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad AdditemPage');
  }

   presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Picture for pet',
      buttons: [
        {
          text: 'Take a Picture',
          role: 'destructive',
          handler: () => {
            this.fromCamera();
          }
        },{
          text: 'Choose from library',
          handler: () => {
            this.fromLibrary();
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  takePhoto(sourceType:number) {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType:sourceType,
    }

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.image = base64Image;
      this.imageURI = imageData;
      this.alertCtrl.create({
        title: 'Image URL',
        message : this.image,
        buttons: ['ok']
      });
    }, (err) => {
      // Handle error
    });
  }

   submitPet(form: NgForm) {
    const loading = this.loadingCtrl.create({
      content: 'Adding your pet...'
    });
    loading.present();

      // imageURI = $scope.src;

    const fileTransfer: FileTransferObject = this.transfer.create();
    
    let options: FileUploadOptions = {
       fileKey: 'photo',
       fileName: 'photo',
       mimeType: 'image/jpeg',
       httpMethod: 'POST',
       params: {
         name:         form.value.name,
         price:        form.value.price,
         description:  form.value.description,
         type:         form.value.type,
         user_id:      this.user_id,
       }
    }

    fileTransfer.upload(this.image, this.baseURL +'api/item/addItemById', options)
     .then((data) => {
       // success
        loading.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Item has been added',
          message: 'Successfully added',
          buttons: ['ok']
        });
      alert.present();
     }, (error) => {
       // error
       error.json();
        console.log(error.json().message);
        loading.dismiss();
        const alert = this.alertCtrl.create({
        title: 'Item add failed',
        message: error.json().message,
        buttons: ['ok']
      });
        alert.present();
     });

    // let postData = new FormData();
    // postData.append('name',form.value.name);
    // postData.append('price',form.value.price);
    // postData.append('description',form.value.description);
    // postData.append('year',form.value.year);
    // postData.append('month',form.value.month);
    // postData.append('type',form.value.type);
    // postData.append('user_id',this.user_id);
    // postData.append('photo',this.image);

    // console.log(form);
    // this.http.post('http://haziq.qbitgroup.com.my/api/pet/addPetById', postData)
    //   .map(res => res.json())
    //   .subscribe(data => {
    //     loading.dismiss();

    //   console.log(data); // data received by server
    //   if(data.status == 200){
    //     const alert = this.alertCtrl.create({
    //     title: 'Pet has been added',
    //     message: data.message,
    //     buttons: ['ok']
    //   });
    //   alert.present();
    //   }
    //   },
    //   error => {
    //     error.json();
    //     console.log(error.json().message);
    //     loading.dismiss();
    //     const alert = this.alertCtrl.create({
    //     title: 'Pet add failed',
    //     message: error.json().message,
    //     buttons: ['ok']
    //   });
    //     alert.present();
    //   });
    // console.log(form);
  }

  fromCamera(){
    this.takePhoto(1);
  }

  fromLibrary(){
    this.takePhoto(0);
  }

}
