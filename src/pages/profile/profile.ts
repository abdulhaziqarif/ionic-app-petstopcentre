import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { CallNumber } from '@ionic-native/call-number';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  user:any;
  constructor(	public navCtrl: NavController, 
  				      public navParams: NavParams,
  				      private callNumber: CallNumber,
                private storage: Storage
                ) {
    // storage.get('currentUser').then((val) => {
    //   this.user = val;
    //   console.log(val);
    // });    
  }

  ionViewDidLoad() {
     this.storage.get('currentUser').then((val) => {
      this.user = val;
      console.log(this.user);
    });    
    
  }

  dialNumber(){
  	this.callNumber.callNumber("18001010101", true)
	  .then(res => console.log('Launched dialer!', res))
	  .catch(err => console.log('Error launching dialer', err));
  }

}
