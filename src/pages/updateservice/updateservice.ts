import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ActionSheetController  } from 'ionic-angular';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

import { NgForm } from '@angular/forms';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

@Component({
  selector: 'page-updateservice',
  templateUrl: 'updateservice.html',
})
export class UpdateservicePage {
pet_id:number;
pet:any;
imageURI:any;
image:string;

  baseURL = 'http://petcentre.haziqarif.review/';

  constructor(		public navCtrl: NavController, 
  				        public navParams: NavParams,
  				        public http: Http,
  				        public loadingCtrl : LoadingController,
             	  	public alertCtrl : AlertController,
                	public storage: Storage,
                	public actionSheetCtrl: ActionSheetController,
                	private camera: Camera,
                	private transfer: FileTransfer ) {

    this.pet_id = this.navParams.get('data');
    console.log(this.pet_id);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdatepetPage');
    this.http.get(this.baseURL+'api/service-detail/'+ this.pet_id)
    .map(res => res.json())
    .subscribe( data => {
    	console.log(data);
    	this.pet = data.data;
    },
    error=> {
      console.log(error);
    });
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Picture for service',
      buttons: [
        {
          text: 'Take a Picture',
          role: 'destructive',
          handler: () => {
            this.fromCamera();
          }
        },{
          text: 'Choose from library',
          handler: () => {
            this.fromLibrary();
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  fromCamera(){
    this.takePhoto(1);
  }

  fromLibrary(){
    this.takePhoto(0);
  }

  takePhoto(sourceType:number) {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType:sourceType,
    }

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.image = base64Image;
      this.imageURI = imageData;
      this.alertCtrl.create({
        title: 'Image URL',
        message : this.image,
        buttons: ['ok']
      });
    }, (err) => {
      // Handle error
    });
  }

   submitPet(form: NgForm) {
     console.log(form);
      const loading = this.loadingCtrl.create({
	      content: 'Updating your service...'
	    });
	    loading.present();

    if(this.image != null){
	      // imageURI = $scope.src;
	      
	    const fileTransfer: FileTransferObject = this.transfer.create();
	    
	    let options: FileUploadOptions = {
	       fileKey: 'photo',
	       fileName: 'photo',
	       mimeType: 'image/jpeg',
	       httpMethod: 'POST',
	       params: {
	         id:         form.value.id,
	         name:         form.value.name,
	         price:        form.value.price,
	         description:  form.value.description,
	         type:         form.value.type
	       }
	    }

	    fileTransfer.upload(this.image, this.baseURL+'api/service/update', options)
	     .then((data) => {
	       // success
	        loading.dismiss();
	        const alert = this.alertCtrl.create({
	          title: 'Service has been updated',
	          message: 'Successfully updated',
	          buttons: ['ok']
	        });
	      alert.present();
	     }, (error) => {
	       // error
	       error.json();
	        console.log(error.json().message);
	        loading.dismiss();
	        const alert = this.alertCtrl.create({
	        title: 'Service update failed',
	        message: error.json().message,
	        buttons: ['ok']
	      });
	        alert.present();
	     });
    }
    else{
    	let postData = new FormData();
	    postData.append('id',form.value.id);
	    postData.append('name',form.value.name);
	    postData.append('price',form.value.price);
	    postData.append('description',form.value.description);
	    postData.append('type',form.value.type);

    	this.http.post(this.baseURL+'api/service/update',postData)
	    .map(res => res.json())
	    .subscribe( data => {
	        loading.dismiss();
	    	console.log(data);
	    	this.pet = data.data;
	    	const alert = this.alertCtrl.create({
	          title: 'Service has been updated',
	          message: 'Successfully updated',
	          buttons: ['ok']
	        });
	      alert.present();
	    },
	    error=> {
	      	error.json();
	        console.log(error.json().message);
	        loading.dismiss();
	        const alert = this.alertCtrl.create({
	        title: 'Service update failed',
	        message: error.json().message,
	        buttons: ['ok']
	      });
	        alert.present();
	    });
    }
   
 }
}
