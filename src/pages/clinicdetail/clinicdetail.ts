import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';

import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { Geolocation } from '@ionic-native/geolocation';
import { CallNumber } from '@ionic-native/call-number';


@Component({
  selector: 'page-clinicdetail',
  templateUrl: 'clinicdetail.html',
})
export class ClinicdetailPage {
	clinic_id:number;
	clinic:any;
  coordinate: {};
  current_lat:any;
  current_lng:any;

  Latitude = undefined;
  Longitude = undefined;
  baseURL = 'http://petcentre.haziqarif.review/';

  constructor(	public navCtrl: NavController, 
        				public navParams: NavParams,
        				public http: Http,
                private launchNavigator: LaunchNavigator,
                private geolocation: Geolocation,
                private callNumber: CallNumber) {
  	 this.clinic_id = this.navParams.get('data');
    console.log(this.clinic_id);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ClinicdetailPage');

    this.http.get(this.baseURL+'api/clinic-detail/'+ this.clinic_id)
    .map(res => res.json())
    .subscribe( data => {
    	console.log(data);
    	this.clinic = data.data;
    },
    error=> {
      console.log(error);
      alert(error);
    });
  }

   navigate(){
     this.geolocation.getCurrentPosition().then((resp) => {
       // resp.coords.latitude = this.current_lat;
       // resp.coords.longitude = this.current_lng;
       // resp.coords = this.coordinate;
        this.Latitude = resp.coords.latitude;
        this.Longitude = resp.coords.longitude;
       console.log(resp.coords);
       console.log(this.Latitude);
      }).catch((error) => {
        console.log('Error getting location', error);
        alert(error);
      });
    let options: LaunchNavigatorOptions = {
      start: [this.Latitude,this.Longitude],
      // start: [this.current_lat,this.current_lng],
      // app: LaunchNavigator.APPS.UBER
    };

    this.launchNavigator.navigate([this.clinic.latitude,this.clinic.longitude], options)
      .then(
        success => console.log('Launched navigator'),
        error => alert(error)
      );
  }

  dialNumber(phone){
    this.callNumber.callNumber(phone, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }
}
