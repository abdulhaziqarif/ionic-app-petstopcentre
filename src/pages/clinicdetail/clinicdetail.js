var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
var ClinicdetailPage = /** @class */ (function () {
    function ClinicdetailPage(navCtrl, navParams, http, launchNavigator) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.launchNavigator = launchNavigator;
        this.clinic_id = this.navParams.get('data');
        console.log(this.clinic_id);
    }
    ClinicdetailPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad ClinicdetailPage');
        this.http.get('http://haziq.qbitgroup.com.my/api/clinic-detail/' + this.clinic_id)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
            _this.clinic = data.data;
        }, function (error) {
            console.log(error);
        });
    };
    ClinicdetailPage.prototype.navigate = function () {
        var options = {
            start: 'London, ON',
        };
        this.launchNavigator.navigate([50.279306, -5.163158], options)
            .then(function (success) { return console.log('Launched navigator'); }, function (error) { return console.log('Error launching navigator', error); });
    };
    ClinicdetailPage = __decorate([
        Component({
            selector: 'page-clinicdetail',
            templateUrl: 'clinicdetail.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            Http,
            LaunchNavigator])
    ], ClinicdetailPage);
    return ClinicdetailPage;
}());
export { ClinicdetailPage };
//# sourceMappingURL=clinicdetail.js.map