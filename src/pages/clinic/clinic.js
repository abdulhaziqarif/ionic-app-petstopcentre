var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { SearchclinicPage } from '../searchclinic/searchclinic';
import { ClinicdetailPage } from '../clinicdetail/clinicdetail';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
var ClinicPage = /** @class */ (function () {
    function ClinicPage(navCtrl, http, loadingCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.clinicdetailpage = ClinicdetailPage;
        this.searchPage = SearchclinicPage;
    }
    ClinicPage.prototype.detailPage = function (pet_id) {
        // console.log(id);
        this.navCtrl.push(this.clinicdetailpage, { data: pet_id });
    };
    ClinicPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var loading = this.loadingCtrl.create();
        loading.present();
        this.http.get('http://haziq.qbitgroup.com.my/api/clinic-list')
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            loading.dismiss();
            console.log(data.data);
            _this.clinic = data.data;
        }, function (error) {
            loading.dismiss();
            console.log(error);
            var alert = _this.alertCtrl.create({
                title: 'Internal Server Error',
                message: error.json().message,
                buttons: ['ok']
            });
            alert.present();
        });
    };
    ClinicPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        this.http.get('http://haziq.qbitgroup.com.my/api/clinic-list')
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data.data);
            _this.clinic = data.data;
        }, function (error) {
            console.log(error);
            var alert = _this.alertCtrl.create({
                title: 'Internal Server Error',
                message: error.json().message,
                buttons: ['ok']
            });
            alert.present();
        });
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    ClinicPage = __decorate([
        Component({
            selector: 'page-clinic',
            templateUrl: 'clinic.html'
        }),
        __metadata("design:paramtypes", [NavController,
            Http,
            LoadingController,
            AlertController])
    ], ClinicPage);
    return ClinicPage;
}());
export { ClinicPage };
//# sourceMappingURL=clinic.js.map