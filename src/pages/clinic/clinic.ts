import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { SearchclinicPage} from '../searchclinic/searchclinic';
import { ClinicdetailPage } from '../clinicdetail/clinicdetail';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { NgForm } from '@angular/forms';


@Component({
  selector: 'page-clinic',
  templateUrl: 'clinic.html'
})
export class ClinicPage {
  searchPage:any;
  clinic: any[];
  clinic2: any[];
  clinicdetailpage = ClinicdetailPage;
  
  public states: any[];
  public districts: any[];
  public cities: any[];

  public selectedDistricts: any[];
  public selectedCities: any[];

  public sState: any;
  public sDistrict: any;
  baseURL = 'http://petcentre.haziqarif.review/';
  
  constructor(	public navCtrl: NavController,
  				      public http: Http,
                public loadingCtrl : LoadingController,
                public alertCtrl : AlertController) {
    this.searchPage = SearchclinicPage;

    this.initializeState();
    this.initializeDistrict();
  }

  detailPage(pet_id){
    // console.log(id);
    this.navCtrl.push(this.clinicdetailpage,{data: pet_id});
  }

  ionViewDidLoad() {
    const loading = this.loadingCtrl.create();
    loading.present();
    this.http.get( this.baseURL+'api/clinic-list')
    .map(res => res.json())
    .subscribe( data => {
      loading.dismiss();
    	console.log(data.data);
    	this.clinic = data.data;
    },
    error=> {
      loading.dismiss();
      console.log(error);
       const alert = this.alertCtrl.create({
        title: 'Internal Server Error',
        message: error.json().message,
        buttons: ['ok']
      });
        alert.present();
    });
	}


  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.http.get(this.baseURL+'api/clinic-list')
    .map(res => res.json())
    .subscribe( data => {
      console.log(data.data);
      this.clinic = data.data;
    },
    error=> {
      console.log(error);
      const alert = this.alertCtrl.create({
        title: 'Internal Server Error',
        message: error.json().message,
        buttons: ['ok']
      });
        alert.present();
    });

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
   initialize(){
    this.clinic2 = this.clinic;
  }

  filterSearch(ev: any){
     // Reset items back to all of the items
    this.initialize();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.clinic = this.clinic.filter((item) => {
        console.log(item);
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  initializeState(){
    this.states = [
        {id: 1, name: 'Melaka'},
        {id: 2, name: 'Johor'},
        {id: 3, name: 'Selangor'}
    ];
    }

    initializeDistrict(){
    this.districts = [
        {id: 1, name: 'Alor Gajah', state_id: 1, state_name: 'Melaka'},
        {id: 2, name: 'Jasin', state_id: 1, state_name: 'Melaka'},
        {id: 3, name: 'Muar', state_id: 2, state_name: 'Johor'},
        {id: 4, name: 'Segamat', state_id: 2, state_name: 'Johor'},
        {id: 6, name: 'Skudai', state_id: 2, state_name: 'Johor'},
        {id: 5, name: 'Shah Alam', state_id: 3, state_name: 'Selangor'},
        {id: 7, name: 'Klang', state_id: 3, state_name: 'Selangor'}
    ];
    }

   setDistrictValues(sState) {
        this.selectedDistricts = this.districts.filter(district => district.state_id == sState.id)
    }

    submitPet(form: NgForm){
    console.log(form);
    const loading = this.loadingCtrl.create({
      content: 'Searching...'
    });
    loading.present();

     let postData = new FormData();
        postData.append('name',form.value.name);
        postData.append('state',form.value.state.name);
        postData.append('town',form.value.town.name);

        this.http.post(this.baseURL+'api/clinic/search', postData)
          .map(res => res.json())
          .subscribe(data => {
            this.clinic = data.data;
            console.log(data.data);
            loading.dismiss();
          },
          error => {
            error.json();
            console.log(error.json().message);
            loading.dismiss();
            const alert = this.alertCtrl.create({
            title: 'Search Fail',
            message: error.json().message,
            buttons: ['ok']
          });
            alert.present();
          });

  }
}
