import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController  } from 'ionic-angular';

import { SearchitemPage } from '../searchitem/searchitem';
import { ServicedetailPage } from '../servicedetail/servicedetail';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
// import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';

import { NgForm } from '@angular/forms';


@Component({
  selector: 'page-service',
  templateUrl: 'service.html',
})
export class ServicePage {
	searchPage:any;
	pet: any;
	petdetailpage = ServicedetailPage;
	isAuthenticated:boolean = false;
  baseURL = 'http://petcentre.haziqarif.review/';

  constructor(	public navCtrl: NavController, 
  				      public navParams: NavParams,
  				      public http: Http,
        		    public events: Events,
                public loadingCtrl : LoadingController,
                public alertCtrl : AlertController) {

  	this.searchPage = SearchitemPage;

    this.events.subscribe('user.login', (condition) => {
        this.isAuthenticated = condition;
        console.log('isAutthenticated');
        console.log(this.isAuthenticated);
      });
  }

 detailPage(pet_id){
  	// console.log(id);
    this.navCtrl.push(this.petdetailpage,{data: pet_id});
  }

  ionViewDidLoad() {   

    const loading = this.loadingCtrl.create();
    loading.present();
    this.http.get(this.baseURL+'api/service-list')
    .map(res => res.json())
    .subscribe( data => {
      loading.dismiss();
    	console.log(data.data);
    	this.pet = data.data;
    },
    error=> {
      loading.dismiss();
      console.log(error);
      const alert = this.alertCtrl.create({
        title: 'Internal Server Error',
        message: error.json().message,
        buttons: ['ok']
      });
        alert.present();
    });
	}

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.http.get(this.baseURL+'api/service-list')
    .map(res => res.json())
    .subscribe( data => {
      console.log(data.data);
      this.pet = data.data;
    },
    error=> {
      console.log(error);
      const alert = this.alertCtrl.create({
        title: 'Internal Server Error',
        message: error.json().message,
        buttons: ['ok']
      });
        alert.present();
    });

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  submitPet(form: NgForm){
    console.log(form);
    const loading = this.loadingCtrl.create({
      content: 'Searching...'
    });
    loading.present();

     let postData = new FormData();
        postData.append('name',form.value.name);
        postData.append('type',form.value.type);
        postData.append('price',form.value.price);

        this.http.post(this.baseURL+'api/service/search', postData)
          .map(res => res.json())
          .subscribe(data => {
            this.pet = data.data;
            loading.dismiss();
          },
          error => {
            error.json();
            console.log(error.json().message);
            loading.dismiss();
            const alert = this.alertCtrl.create({
            title: 'Search Fail',
            message: error.json().message,
            buttons: ['ok']
          });
            alert.present();
          });

  }

}
