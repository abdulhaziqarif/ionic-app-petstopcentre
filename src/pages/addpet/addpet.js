var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ActionSheetController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { FileTransfer } from '@ionic-native/file-transfer';
var AddpetPage = /** @class */ (function () {
    function AddpetPage(navCtrl, navParams, http, loadingCtrl, alertCtrl, storage, actionSheetCtrl, camera, transfer, file) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.transfer = transfer;
        this.file = file;
    }
    AddpetPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddpetPage');
    };
    AddpetPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Picture for pet',
            buttons: [
                {
                    text: 'Take a Picture',
                    role: 'destructive',
                    handler: function () {
                        _this.fromCamera();
                    }
                }, {
                    text: 'Choose from library',
                    handler: function () {
                        _this.fromLibrary();
                    }
                }, {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    AddpetPage.prototype.takePhoto = function (sourceType) {
        var _this = this;
        var options = {
            quality: 50,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            sourceType: sourceType,
        };
        this.camera.getPicture(options).then(function (imageData) {
            var base64Image = 'data:image/jpeg;base64,' + imageData;
            _this.image = base64Image;
            _this.alertCtrl.create({
                title: 'Image URL',
                message: _this.image,
                buttons: ['ok']
            });
        }, function (err) {
            // Handle error
        });
    };
    AddpetPage.prototype.submitPet = function (form) {
        var _this = this;
        this.storage.get('currentUser').then(function (val) {
            _this.user_id = val.id;
        });
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: 'photo',
            fileName: form.value.name + this.user_id + '.jpg',
            mimeType: 'image/jpeg',
            httpMethod: 'POST',
            params: {
                name: name
            }
        };
        fileTransfer.upload('<file path>', '<api endpoint>', options)
            .then(function (data) {
            // success
        }, function (err) {
            // error
        });
        var postData = new FormData();
        postData.append('name', form.value.name);
        postData.append('price', form.value.price);
        postData.append('description', form.value.description);
        postData.append('year', form.value.year);
        postData.append('month', form.value.month);
        postData.append('type', form.value.type);
        postData.append('type', form.value.type);
        postData.append('user_id', this.user_id);
        postData.append('photo', this.image);
        // console.log(form);
        var loading = this.loadingCtrl.create({
            content: 'Adding your pet...'
        });
        loading.present();
        this.http.post('http://haziq.qbitgroup.com.my/api/pet/addPetById', postData)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            loading.dismiss();
            console.log(data); // data received by server
            if (data.status == 200) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Pet has been added',
                    message: data.message,
                    buttons: ['ok']
                });
                alert_1.present();
            }
        }, function (error) {
            error.json();
            console.log(error.json().message);
            loading.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Pet add failed',
                message: error.json().message,
                buttons: ['ok']
            });
            alert.present();
        });
        console.log(form);
    };
    AddpetPage.prototype.fromCamera = function () {
        this.takePhoto(1);
    };
    AddpetPage.prototype.fromLibrary = function () {
        this.takePhoto(0);
    };
    AddpetPage.prototype.upload = function () {
        var fileTransfer = this.transfer.create();
        var options = __assign({ fileKey: 'file', fileName: 'name.jpg', headers: {} }, ..
        );
        fileTransfer.upload('<file path>', '<api endpoint>', options)
            .then(function (data) {
            // success
        }, function (err) {
            // error
        });
    };
    AddpetPage = __decorate([
        Component({
            selector: 'page-addpet',
            templateUrl: 'addpet.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            Http,
            LoadingController,
            AlertController,
            Storage,
            ActionSheetController,
            Camera,
            FileTransfer, File])
    ], AddpetPage);
    return AddpetPage;
}());
export { AddpetPage };
//# sourceMappingURL=addpet.js.map