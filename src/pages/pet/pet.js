var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { SearchpetPage } from '../searchpet/searchpet';
import { PetdetailPage } from '../petdetail/petdetail';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
// import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';
var PetPage = /** @class */ (function () {
    function PetPage(navCtrl, http, events, loadingCtrl, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.http = http;
        this.events = events;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.petdetailpage = PetdetailPage;
        this.isAuthenticated = false;
        this.searchPage = SearchpetPage;
        this.events.subscribe('user.login', function (condition) {
            _this.isAuthenticated = condition;
            console.log('isAutthenticated');
            console.log(_this.isAuthenticated);
        });
    }
    PetPage.prototype.detailPage = function (pet_id) {
        // console.log(id);
        this.navCtrl.push(this.petdetailpage, { data: pet_id });
    };
    PetPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var loading = this.loadingCtrl.create();
        loading.present();
        this.http.get('http://haziq.qbitgroup.com.my/api/pet-list')
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            loading.dismiss();
            console.log(data.data);
            _this.pet = data.data;
        }, function (error) {
            loading.dismiss();
            console.log(error);
            var alert = _this.alertCtrl.create({
                title: 'Internal Server Error',
                message: error.json().message,
                buttons: ['ok']
            });
            alert.present();
        });
    };
    PetPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        this.http.get('http://haziq.qbitgroup.com.my/api/pet-list')
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data.data);
            _this.pet = data.data;
        }, function (error) {
            console.log(error);
            var alert = _this.alertCtrl.create({
                title: 'Internal Server Error',
                message: error.json().message,
                buttons: ['ok']
            });
            alert.present();
        });
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    PetPage = __decorate([
        Component({
            selector: 'page-pet',
            templateUrl: 'pet.html'
        }),
        __metadata("design:paramtypes", [NavController,
            Http,
            Events,
            LoadingController,
            AlertController])
    ], PetPage);
    return PetPage;
}());
export { PetPage };
//# sourceMappingURL=pet.js.map