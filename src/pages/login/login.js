var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
// import { HTTP } from '@ionic-native/http';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { RegisterPage } from '../register/register';
import { IntroPage } from '../intro/intro';
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, http, loadingCtrl, alertCtrl, storage, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.events = events;
        this.introPage = IntroPage;
        this.tabPage = TabsPage;
        this.isAuthenticated = false;
        this.nextPage = TabsPage;
        this.registerPage = RegisterPage;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.logForm = function (form) {
        var _this = this;
        var postData = new FormData();
        postData.append('email', form.value.email);
        postData.append('password', form.value.password);
        // console.log(postData)
        var loading = this.loadingCtrl.create({
            content: 'Login you up...'
        });
        loading.present();
        this.http.post('http://haziq.qbitgroup.com.my/api/api-login', postData)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            loading.dismiss();
            console.log(data); // data received by server
            if (data.status == 200) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Login success',
                    message: data.message,
                    buttons: ['ok']
                });
                _this.storage.set('currentUser', data.data);
                _this.storage.get('currentUser').then(function (val) {
                    // console.log(val);
                });
                _this.events.publish('user.login', true);
                _this.navCtrl.setRoot(_this.tabPage);
                alert_1.present();
            }
        }, function (error) {
            error.json();
            console.log(error.json().message);
            loading.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Login failed',
                message: error.json().message,
                buttons: ['ok']
            });
            alert.present();
        });
        // console.log(form);
    };
    LoginPage = __decorate([
        Component({
            selector: 'page-login',
            templateUrl: 'login.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            Http,
            LoadingController,
            AlertController,
            Storage,
            Events])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.js.map