import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { NgForm } from '@angular/forms';

// import { HTTP } from '@ionic-native/http';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

import { Events } from 'ionic-angular';

import { TabsPage } from '../tabs/tabs';
import { RegisterPage } from '../register/register';
import { IntroPage } from '../intro/intro';





@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  nextPage:any;
  registerPage: any;
  introPage = IntroPage;
  tabPage = TabsPage;
  isAuthenticated = false;
  baseURL = 'http://petcentre.haziqarif.review/';
  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private http: Http, 
              public loadingCtrl : LoadingController,
              public alertCtrl : AlertController,
              private storage: Storage,
              public events: Events) {
    this.nextPage = TabsPage;
    this.registerPage = RegisterPage;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  logForm(form: NgForm) {

    let postData = new FormData();
    postData.append('email',form.value.email);
    postData.append('password',form.value.password);

    // console.log(postData)

    const loading = this.loadingCtrl.create({
      content: 'Login you up...'
    });
    loading.present();
    this.http.post(this.baseURL+'api/api-login', postData)
      .map(res => res.json())
      .subscribe(data => {
        loading.dismiss();

      console.log(data); // data received by server
      if(data.status == 200){
        const alert = this.alertCtrl.create({
        title: 'Login success',
        message: data.message,
        buttons: ['ok']
      });
        this.storage.set('currentUser', data.data);
        this.storage.get('currentUser').then((val) => {
          // console.log(val);
        });
        this.events.publish('user.login', true);
        this.navCtrl.setRoot(this.tabPage);
      alert.present();

      }
      },
      error => {
        error.json();
        console.log(error.json().message);
        loading.dismiss();
        const alert = this.alertCtrl.create({
        title: 'Login failed',
        message: error.json().message,
        buttons: ['ok']
      });
        alert.present();
      });
    // console.log(form);
  }

}
