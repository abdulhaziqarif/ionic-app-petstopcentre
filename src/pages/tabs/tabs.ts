import { Component } from '@angular/core';

import { PetPage } from '../pet/pet';
import { ClinicPage } from '../clinic/clinic';
import { ShopPage } from '../shop/shop';
import { ShelterPage } from '../shelter/shelter';
import { NewsPage } from '../news/news';
import { SearchclinicPage } from '../searchclinic/searchclinic';
import { ItemPage } from '../item/item';
import { ServicePage } from '../service/service';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = PetPage;
  tab2Root = ItemPage;
  tab3Root = ServicePage;
  tab4Root = ShopPage;
  tab5Root = ClinicPage;
  tab6Root = ShelterPage;

  constructor() {

  }
}
