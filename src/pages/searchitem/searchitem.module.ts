import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchitemPage } from './searchitem';

@NgModule({
  declarations: [
    SearchitemPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchitemPage),
  ],
})
export class SearchitemPageModule {}
