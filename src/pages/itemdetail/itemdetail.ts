import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-itemdetail',
  templateUrl: 'itemdetail.html',
})
export class ItemdetailPage {
	pet_id:number;
	pet:any;
  baseURL = 'http://petcentre.haziqarif.review/';
  constructor(	public navCtrl: NavController, 
  				public navParams: NavParams,
      			public http: Http) {
  	this.pet_id = this.navParams.get('data');
    console.log(this.pet_id);
  }

   ionViewDidLoad() {
    console.log('ionViewDidLoad PetdetailPage');
    console.log('Pet Detail Page');
    console.log(this.pet_id);	

    this.http.get(this.baseURL+'api/item-detail/'+ this.pet_id)
    .map(res => res.json())
    .subscribe( data => {
    	console.log(data);
    	this.pet = data.data;
    },
    error=> {
      console.log(error);
    });

  }

}
