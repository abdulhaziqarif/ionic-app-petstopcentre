var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
var ItemdetailPage = /** @class */ (function () {
    function ItemdetailPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.pet_id = this.navParams.get('data');
        console.log(this.pet_id);
    }
    ItemdetailPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad PetdetailPage');
        console.log('Pet Detail Page');
        console.log(this.pet_id);
        this.http.get('http://haziq.qbitgroup.com.my/api/item-detail/' + this.pet_id)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
            _this.pet = data.data;
        }, function (error) {
            console.log(error);
        });
    };
    ItemdetailPage = __decorate([
        Component({
            selector: 'page-itemdetail',
            templateUrl: 'itemdetail.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            Http])
    ], ItemdetailPage);
    return ItemdetailPage;
}());
export { ItemdetailPage };
//# sourceMappingURL=itemdetail.js.map