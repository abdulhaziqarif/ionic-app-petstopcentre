var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
var ShelterdetailPage = /** @class */ (function () {
    function ShelterdetailPage(navCtrl, navParams, http, launchNavigator) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.launchNavigator = launchNavigator;
        this.shelter_id = this.navParams.get('data');
        console.log(this.shelter_id);
    }
    ShelterdetailPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad ShelterdetailPage');
        if (this.shelter_id == 0) {
            var data = {
                name: "Test Shelter",
                photo: "../assets/imgs/bg-test.jpg",
                latitude: 50.279306,
                longitude: -5.163158
            };
            this.shelter = data;
        }
        else {
            this.http.get('http://haziq.qbitgroup.com.my/api/shelter-detail/' + this.shelter_id)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log(data);
                _this.shelter = data.data;
            }, function (error) {
                console.log(error);
            });
        }
    };
    ShelterdetailPage.prototype.navigate = function () {
        var options = {
            start: 'UTM, Malaysia',
        };
        this.launchNavigator.navigate([this.shelter.latitude, this.shelter.longitude], options)
            .then(function (success) { return console.log('Launched navigator'); }, function (error) { return console.log('Error launching navigator', error); });
    };
    ShelterdetailPage = __decorate([
        Component({
            selector: 'page-shelterdetail',
            templateUrl: 'shelterdetail.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            Http,
            LaunchNavigator])
    ], ShelterdetailPage);
    return ShelterdetailPage;
}());
export { ShelterdetailPage };
//# sourceMappingURL=shelterdetail.js.map