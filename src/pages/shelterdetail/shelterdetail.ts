import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';

import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { Geolocation } from '@ionic-native/geolocation';
import { CallNumber } from '@ionic-native/call-number';

@Component({
  selector: 'page-shelterdetail',
  templateUrl: 'shelterdetail.html',
})
export class ShelterdetailPage {
	shelter_id:number;
	shelter:any;
  current_lat:any;
  current_lng:any;

  Latitude = undefined;
  Longitude = undefined;

  baseURL = 'http://petcentre.haziqarif.review/';
  constructor(	public navCtrl: NavController, 
  				      public navParams: NavParams,
  				      public http: Http,
                private launchNavigator: LaunchNavigator,
                private geolocation: Geolocation,
                private callNumber: CallNumber) {
  	this.shelter_id = this.navParams.get('data');
    console.log(this.shelter_id);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShelterdetailPage');

    if(this.shelter_id == 0){
      let data = {
        name : "Test Shelter",
        photo : "../assets/imgs/bg-test.jpg",
        latitude : 50.279306,
        longitude : -5.163158
      }
      this.shelter = data;
    }else{
      this.http.get(this.baseURL+'api/shelter-detail/'+ this.shelter_id)
      .map(res => res.json())
      .subscribe( data => {
        console.log(data);
        this.shelter = data.data;
      },
      error=> {
        console.log(error);
        alert(error);
      });
    }

     
  }

  navigate(){
    
    this.geolocation.getCurrentPosition().then((resp) => {
        this.Latitude = resp.coords.latitude;
        this.Longitude = resp.coords.longitude;
      }).catch((error) => {
        console.log('Error getting location', error);
        alert(error);
      });
    let options: LaunchNavigatorOptions = {
      start: [this.Latitude,this.Longitude],
      // start: [this.current_lat,this.current_lng],
      // app: LaunchNavigator.APPS.UBER
    };

    this.launchNavigator.navigate([this.shelter.latitude, this.shelter.longitude], options)
      .then(
        success => console.log('Launched navigator'),
        error => alert(error)
      );
  }

  dialNumber(phone){
    this.callNumber.callNumber(phone, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

}
