var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { AddpetPage } from '../addpet/addpet';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
var MypetPage = /** @class */ (function () {
    function MypetPage(navCtrl, navParams, alertCtrl, loadingCtrl, http, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.storage = storage;
        this.addPage = AddpetPage;
        this.storage.get('currentUser').then(function (val) {
            _this.user_id = val.id;
            console.log(_this.user_id);
        });
    }
    MypetPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad MypetPage');
        var loading = this.loadingCtrl.create();
        loading.present();
        this.http.get('http://haziq.qbitgroup.com.my/api/pet-list/individual/' + this.user_id)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            loading.dismiss();
            console.log(data.data);
            _this.pet = data.data;
        }, function (error) {
            loading.dismiss();
            console.log(error);
            var alert = _this.alertCtrl.create({
                title: 'Internal Server Error',
                message: error.json().message,
                buttons: ['ok']
            });
            alert.present();
        });
    };
    MypetPage = __decorate([
        Component({
            selector: 'page-mypet',
            templateUrl: 'mypet.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            AlertController,
            LoadingController,
            Http,
            Storage])
    ], MypetPage);
    return MypetPage;
}());
export { MypetPage };
//# sourceMappingURL=mypet.js.map