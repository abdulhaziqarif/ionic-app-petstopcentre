import { Component } from '@angular/core';
import { NavController, NavParams,LoadingController, AlertController } from 'ionic-angular';
import { NgForm } from '@angular/forms';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

import { TabsPage } from '../tabs/tabs';


@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  baseURL = 'http://petcentre.haziqarif.review/';
  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private http: Http, 
              public loadingCtrl : LoadingController,
              public alertCtrl : AlertController,
              private storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  signupForm(form: NgForm) {
    let postData = new FormData();
    postData.append('username', form.value.username);
    postData.append('email', form.value.email);
    postData.append('password', form.value.password);
    postData.append('roles', form.value.roles);
    console.log(form.value);
    const loading = this.loadingCtrl.create({
      content: 'Creating your account...'
    });
    loading.present();
    this.http.post(this.baseURL+'api/api-register', postData)
    .map(res => res.json())
    .subscribe( data => {
      loading.dismiss();
      console.log(data);

      if(data.status == 200 ){
        
        const alert = this.alertCtrl.create({
          title: 'Registration success',
          message: data.message,
          buttons: ['ok']
        });
        this.navCtrl.setRoot(TabsPage);
        alert.present();
      }      

    }, error=> {
      loading.dismiss();
      console.log(error);
        const alert = this.alertCtrl.create({
        title: 'Registration failed',
        message: error.json().data.email,
        buttons: ['ok']
      });
        alert.present();
    });
  }

}
