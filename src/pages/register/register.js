var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';
var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, navParams, http, loadingCtrl, alertCtrl, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.signupForm = function (form) {
        var _this = this;
        var postData = new FormData();
        postData.append('username', form.value.username);
        postData.append('email', form.value.email);
        postData.append('password', form.value.password);
        postData.append('roles', form.value.roles);
        console.log(form.value);
        var loading = this.loadingCtrl.create({
            content: 'Creating your account...'
        });
        loading.present();
        this.http.post('http://haziq.qbitgroup.com.my/api/api-register', postData)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            loading.dismiss();
            console.log(data);
            if (data.status == 200) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Registration success',
                    message: data.message,
                    buttons: ['ok']
                });
                _this.navCtrl.setRoot(TabsPage);
                alert_1.present();
            }
        }, function (error) {
            loading.dismiss();
            console.log(error);
            var alert = _this.alertCtrl.create({
                title: 'Registration failed',
                message: error.json().data.email,
                buttons: ['ok']
            });
            alert.present();
        });
    };
    RegisterPage = __decorate([
        Component({
            selector: 'page-register',
            templateUrl: 'register.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            Http,
            LoadingController,
            AlertController,
            Storage])
    ], RegisterPage);
    return RegisterPage;
}());
export { RegisterPage };
//# sourceMappingURL=register.js.map