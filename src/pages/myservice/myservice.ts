import { Component } from '@angular/core';
import { NavController, NavParams,LoadingController, AlertController } from 'ionic-angular';

import { AddservicePage } from '../addservice/addservice';
import { ServicedetailPage } from '../servicedetail/servicedetail';
import { UpdateservicePage } from '../updateservice/updateservice';


import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-myservice',
  templateUrl: 'myservice.html',
})
export class MyservicePage {
	pet: any;
	user_id:number;
	addPage:any;
  petdetailpage = ServicedetailPage;
  updatepetpage = UpdateservicePage;
  baseURL = 'http://petcentre.haziqarif.review/';

  constructor(	public navCtrl: NavController, 
  				      public navParams: NavParams,
				        public alertCtrl : AlertController,
        				public loadingCtrl : LoadingController,
        				public http: Http,
        				private storage: Storage) {
  	 this.addPage = AddservicePage;
  	 this.storage.get('currentUser').then((val) => {
          this.user_id = val.id;
          console.log(this.user_id);
        });
     this.getPet(this.user_id);
  }

  getPet(userID) {   

    const loading = this.loadingCtrl.create();
    loading.present();
    this.http.get(this.baseURL+'api/service-list/individual/'+userID)
    .map(res => res.json())
    .subscribe( data => {
      console.log(data.data);
      this.pet = data.data;
      loading.dismiss();
      
    },
    error=> {
      loading.dismiss();
      console.log(error);
      const alert = this.alertCtrl.create({
        title: 'Internal Server Error',
        message: error.json().message,
        buttons: ['ok']
      });
        alert.present();
    });
  }

   doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.http.get(this.baseURL+'api/service-list/individual/'+this.user_id)
    .map(res => res.json())
    .subscribe( data => {
      console.log(data.data);
      this.pet = data.data;
    },
    error=> {
      console.log(error);
      const alert = this.alertCtrl.create({
        title: 'Internal Server Error',
        message: error.json().message,
        buttons: ['ok']
      });
        alert.present();
    });

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  detailPage(pet_id){
    // console.log(id);
    this.navCtrl.push(this.petdetailpage,{data: pet_id});
  }

  editPage(pet_id){
    // console.log(id);
    this.navCtrl.push(this.updatepetpage,{data: pet_id});
  }  

  deletePet(pet_id){
    let alert = this.alertCtrl.create({
    title: 'Confirm delete',
    message: 'Do you sure to delete this pet?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
          // alert.dismiss();  
        }
      },
      {
        text: 'Delete',
        handler: () => {
          console.log('Delete clicked');
          const loading = this.loadingCtrl.create();
          loading.present();
          this.http.get(this.baseURL+'api/service-delete/individual/'+pet_id)
            .map(res => res.json())
            .subscribe( data => {
              console.log(data.data);
              loading.dismiss();
            },
            error=> {
              console.log(error);
              loading.dismiss();
              const alert2 = this.alertCtrl.create({
                title: 'Internal Server Error',
                message: error.json().message,
                buttons: ['ok']
              });
                alert2.present();
            });

        }
      }
    ]
  });
  alert.present();
  }

}
